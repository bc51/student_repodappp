export const STUDENTRECORD_ADDRESS = '0xa92bE594aB37dcBc451BBB422212906c3cc6c3a6'
export const STUDENTRECORD_ABI= [
{
"inputs": [],
"stateMutability": "nonpayable",
"type": "constructor"
},
{
"anonymous": false,
"inputs": [
{
"indexed": false,
"internalType": "uint256",
"name": "_id",
"type": "uint256"
},
{
"indexed": true,
"internalType": "uint256",
"name": "sid",
"type": "uint256"
},
{
"indexed": false,
"internalType": "string",
"name": "name",
"type": "string"
},
{
"indexed": false,
"internalType": "bool",
"name": "graduated",
"type": "bool"
}
],
"name": "addStudentEvent",
"type": "event"
},
{
"anonymous": false,
"inputs": [
{
"indexed": true,
"internalType": "uint256",
"name": "sid",
"type": "uint256"
}
],
"name": "markGraduatedEvent",
"type": "event"}]
